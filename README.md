#### Программа 
"task-manager" ver 1.0.2                                                                                                        
##### Требования к Software
 Open JDK 11 
                                                                                                                    
##### Стек технологий
 Apache Maven 3.6.1
##### Разработчик
 nalimov_sv@nlmk.com
##### Сборка приложения
mvn clean \ mvn install
 ##### Запуск приложения
```
jse-03>java -jar ./target/task-manager-1.0.2.jar help   
```
version - Display program version.
about - Display developer info.
help - Display list of terminal commands.   